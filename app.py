from contextlib import closing

import psycopg2
from faker import Faker
from psycopg2.extras import DictCursor


DB_NAME = 'floberis'

DB_USER = 'postgres'

DB_PASSWORD = '123456'

DB_HOST = 'localhost'

DB_PORT = 9452

fake = Faker('ru_RU')


def main():
    with closing(psycopg2.connect(dbname=DB_NAME, user=DB_USER, port=DB_PORT,
                                  password=DB_PASSWORD, host=DB_HOST)) as conn:
        conn.autocommit = True
        with conn.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute('SELECT user_id FROM main_member')
            records = cursor.fetchall()
            for row in records:
                cursor.execute(
                    '''UPDATE main_member SET name=%(name)s, email=%(email)s, phone=%(phone)s WHERE user_id=%(user_id)s;''',
                    {
                        'user_id': row['user_id'],
                        'name': fake.name(),
                        'email': fake.email(),
                        'phone': fake.phone_number()
                    }
                )


if __name__ == '__main__':
    main()
